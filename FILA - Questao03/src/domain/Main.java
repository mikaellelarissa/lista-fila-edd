package domain;

import java.util.Queue;
import java.util.LinkedList;
import java.util.Scanner;

public class Main {

	private static Scanner scan = new Scanner(System.in);
	private static Queue<Cliente> filaClientes = new LinkedList<Cliente>();
	private static Integer esc = 0;
	private static String nome;

	public static void main(String[] args) {		
		do {
			System.out.println("FILA SUPERMERCADO");
			System.out.println("(1) Inserir cliente \n(2) Remover cliente \n(3) Consultar quantidade de clientes na fila \n(4) Sair");
			esc = scan.nextInt();
			scan.nextLine();
			switch (esc) {
			case 1: {
				filaClientes.add(inserirCliente(nome));
				break;
			}
			case 2: {
				removerCliente();
				break;
			}
			case 3:{
				consultarCliente();
				break;
			}
			default:
				System.out.println("At�!");
			}
		} while (esc != 4);

	}

	public static Cliente inserirCliente(String nome) {
		System.out.println("Informe o nome do cliente: ");
		nome = scan.nextLine();
		return new Cliente(nome);
	}

	public static void removerCliente() {
		if(filaClientes.size() > 0) {
			filaClientes.remove();
		}
	}

	public static void consultarCliente() {
		System.out.println("A quantidade de clientes na fila �: " + filaClientes.size());
	}
}
