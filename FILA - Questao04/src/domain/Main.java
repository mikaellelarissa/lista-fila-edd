package domain;

import java.util.Queue;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Main {

	private static Scanner scan = new Scanner(System.in);
	private static Queue<Aluno> filaAlunos = new LinkedList<Aluno>();
	private static Integer esc = 0;
	private static Integer idade;
	private static String nome;
	
	public static void main(String[] args) {
		do {
			System.out.println("FILA ALUNOS");
			System.out.println("(1) Inserir aluno \n(2) Consultar aluno \n(3) Sair");
			esc = scan.nextInt();
			scan.nextLine();
			switch (esc) {
			case 1: {
				inserirAluno(nome, idade);
				break;
			}
			case 2: {
				consultarAluno();
				break;
			}
			default:
				System.out.println("At�!");
			}
		} while (esc != 3);

	}

	public static void inserirAluno(String nome, Integer idade) {
		System.out.println("Informe o nome do aluno: ");
		nome = scan.nextLine();
		System.out.println("Informe a idade do aluno: ");
		idade = scan.nextInt();
		scan.nextLine();
		
		Aluno aluno = new Aluno(nome, idade);
		filaAlunos.add(aluno);
		sorted();
	}
	
	public static void consultarAluno() {
		System.out.println("(1) Consultar aluno \n(2) Consultar nomes dos alunos que est�o na fila \n(3) Consultar quantidade de alunos na fila");
		Integer op = scan.nextInt();
		scan.nextLine();
		if(op == 1) {
			System.out.println("Informe o nome e a idade do aluno que deseja consultar: ");
			nome = scan.nextLine();
			idade = scan.nextInt();
			scan.nextLine();
			for(Aluno aluno : filaAlunos) {
				if(aluno.getNome().equals(nome) && aluno.getIdade().equals(idade)) {
					System.out.println("Aluno: " + nome + " Idade: " + idade + "\nPosi��o: " + aluno);
				} else {
					System.out.println("Aluno n�o encontrado na fila!");
				}
			}
		} else if (op == 2) {
			System.out.println("Os alunos na fila s�o: ");
			Iterator<Aluno> it = filaAlunos.iterator();
			while(it.hasNext()) {
				System.out.println(it.next());
			}
		} else if (op == 3) {
			System.out.println("A quantidade de alunos na fila �: " + filaAlunos.size());
		}
	}
	
	private static void sorted() {
		List<Aluno> lista = new ArrayList<Aluno>();
		for (Aluno aluno : filaAlunos) {
			lista.add(aluno);
		}
		lista.sort((a,b) -> Integer.compare(a.getIdade(), b.getIdade()));
		filaAlunos.clear();
		for (Aluno aluno : lista) {
			filaAlunos.add(aluno);
		}
	}

}
