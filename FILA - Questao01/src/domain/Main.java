package domain;

import java.util.Queue;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.Iterator;

public class Main {

	private static Scanner scan = new Scanner(System.in);
	private static Queue<Musica> filaMusicas = new LinkedList<Musica>();
	private static Integer esc = 0;
	private static String nome;
	
	public static void main(String[] args) {
		System.out.println("PLAYLIST DE M�SICAS");
		do {
			System.out.println("(1) Inserir m�sica \n(2) Consultar m�sica \n(3) Remover m�sica \n(4) Sair");
			esc = scan.nextInt();
			scan.nextLine();
			switch (esc) {
			case 1: {
				filaMusicas.add(inserirMusica(nome));
				break;
			}
			case 2: {
				consultarMusica(nome);
				break;
			}
			case 3: {
				removerMusica();
				break;
			}
			default:
				System.out.println("At�!");
			}
		} while (esc != 4);

	}

	public static Musica inserirMusica(String nome) {
		System.out.println("Informe o nome da m�sica que deseja adicionar a playlist: ");
		nome = scan.nextLine();
		return new Musica(nome);
	}

	public static void consultarMusica(String nome) {
		System.out.println("(1) Consultar m�sica espec�fica \n(2) Consultar lista de m�sica \n(3) Consultar quantidade de m�sicas");
		Integer op = scan.nextInt();
		scan.nextLine();
		if(op == 1) {
			System.out.println("Informe a m�sica que deseja consultar: ");
			nome = scan.nextLine();
			for(Musica musica : filaMusicas) {
				if(musica.getNome().equals(nome)) {
					System.out.println("M�sica: " + nome + "\nPosi��o: " + musica);
				}
			}
		} else if (op == 2) {
			Iterator<Musica> it = filaMusicas.iterator();
			while(it.hasNext()) {
				System.out.println(it.next());
			}
		} else if (op == 3) {
			System.out.println("A quantidade de m�sicas da playlist �: " + filaMusicas.size());
		} else {
			System.out.println("Inv�lido!");
		}
	}
	
	public static void removerMusica() {
		if(filaMusicas.size() > 0) {
			filaMusicas.remove();
		}
	}
}
