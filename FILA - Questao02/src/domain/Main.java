package domain;

import java.util.Queue;
import java.util.LinkedList;
import java.util.Scanner;

public class Main {

	private static Scanner scan = new Scanner(System.in);
	private static Queue<String> filaContatos = new LinkedList<String>();
	private static Integer esc = 0;
	private static String nome;
	private static String numero;
	
	public static void main(String[] args) {
		do {
			System.out.println("CONTATOS");
			System.out.println("(1) Inserir Contato \n(2) Pr�ximo Contato \n(3) Sair");
			esc = scan.nextInt();
			scan.nextLine();
			switch (esc) {
			case 1: {
				filaContatos.add(inserirContato(nome, numero));
				break;
			}
			case 2: {
				proximoContato();
				break;
			}
			default:
				System.out.println("At�!");
			}
		} while (esc != 3);

	}
	
	public static String inserirContato(String nome, String numero) {
		System.out.println("Informe o nome do contato: ");
		nome = scan.nextLine();
		System.out.println("Informe o n�mero do contato: ");
		numero = scan.nextLine();
		return nome + " " + numero;
	}
	
	public static void proximoContato() {
		System.out.println("Removendo... " + filaContatos.peek());
		if(filaContatos.size() > 0) {
			filaContatos.remove();
		}
		System.out.println("Pr�ximo contato: " + filaContatos.peek());
	}
}
