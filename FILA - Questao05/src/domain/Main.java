package domain;

import java.util.Queue;
import java.util.Scanner;
import java.util.LinkedList;

public class Main {
	
	private static Scanner scan = new Scanner(System.in);
	private static Queue<String> filaPacientes = new LinkedList<String>();
	private static Integer esc = 0;
	private static String nome;
	
	public static void main(String[] args) {
		do {
			System.out.println("FILA DE PACIENTES");
			System.out.println("(1) Inserir paciente \n(2) Atender paciente \n(3) Verificar se h� pacientes \n(4) Pr�ximo paciente \n(5) Quantidade de pacientes que aguardam atendimento \n(6) Sair");
			esc = scan.nextInt();
			scan.nextLine();
			switch (esc) {
			case 1: {
				filaPacientes.add(inserirPaciente(nome));
				break;
			}
			case 2: {
				atenderPaciente();
				break;
			}
			case 3: {
				verificarPacientes();
				break;
			}
			case 4: {
				proximoPaciente();
				break;
			}
			case 5: {
				quantidadePacientes();
				break;
			}
			default:
				System.out.println("At�!");
			}
		} while (esc != 6);
	}

	public static String inserirPaciente(String nome) {
		System.out.println("Informe o nome do paciente a inserir na fila: ");
		nome = scan.nextLine();
		return nome;
	}
	
	public static void atenderPaciente() {
		if(filaPacientes.size() > 0) {
			filaPacientes.remove();
		}
	}
	
	public static void verificarPacientes() {
		if(filaPacientes.size() > 0) {
			System.out.println("H� pacientes na fila para serem atendidos!");
		} else {
			System.out.println("N�o h� pacientes na fila para serem atendidos!");
		}
	}
	
	public static void proximoPaciente() {
		System.out.println(filaPacientes.peek());
	}
	
	public static void quantidadePacientes() {
		System.out.println("A quantidade de clientes para serem atentidos �: " + filaPacientes.size());
	}
}
